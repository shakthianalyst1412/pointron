import Session from "$lib/local/components/session/Session.svelte";
import {
  BlockType,
  PaintType,
  ThinModeBehavior,
  type ComponentType,
} from "$lib/tidy/types/component.type";
import Flow from "$lib/local/components/flow/Flow.svelte";
import Point from "$lib/local/components/point/Point.svelte";
import AppearanceBasics from "$lib/local/components/settings/AppearanceBasics.svelte";
import TagsAndRoutines from "$lib/local/components/settings/TagsAndRoutines.svelte";
import TrackingSettings from "$lib/local/components/settings/TrackingSettings.svelte";
import AboutSettings from "$lib/local/components/settings/AboutSettings.svelte";
import Timeline from "../components/timeline/Timeline.svelte";
import StorageSettings from "$lib/local/components/settings/StorageSettings.svelte";
import SessionSettings from "$lib/local/components/settings/SessionSettings.svelte";
import SessionMiniPlayer from "../components/point/SessionMiniPlayer.svelte";
import { resolvePointPageMode } from "../utils/routeActions";
import MinimalPoint from "../components/point/MinimalPoint.svelte";
import BigTime from "../components/point/BigTime.svelte";
import SettingsFooter from "../components/settings/SettingsFooter.svelte";
import ExpenseTracker from "../components/expenseTracker/ExpenseTracker.svelte";

export const localComponents: ComponentType[] = [
  {
    path: "point",
    action: resolvePointPageMode,
    icon: "point",
  },
  {
    path: "point/journal",
    sections: ["timeline", "session", "bigtime"],
    pagePaint: PaintType.PANEL_ON_LEFT,
    thinModeBehavior: ThinModeBehavior.RIGHT_PANEL_AS_PLAYER,
  },
  {
    path: "point/minimal",
    component: MinimalPoint,
  },
  {
    path: "timeline",
    component: Timeline,
  },
  {
    path: "session",
    component: Session,
    associatedPlayer: "pointron.miniplayer",
  },
  {
    path: "bigtime",
    component: BigTime,
  },
  {
    path: "pointron.miniplayer",
    component: SessionMiniPlayer,
  },
  {
    path: "flow",
    component: Flow,
    icon: "flow",
  },
  {
    heading: "Settings",
    icon: "control",
    path: "settings",
    sections: [
      "appearance",
      "session",
      "tags",
      "tracking",
      "data",
      "about",
      "footer",
    ],
    pagePaint: PaintType.YMENU,
    thinModeBehavior: ThinModeBehavior.GRAND_CHILDREN_ON_MENU,
  },
  {
    heading: "Basics",
    path: "settings/appearance/basics",
    component: AppearanceBasics,
    pagePaint: PaintType.JUMP_TO_PARENT,
    type: BlockType.SECTION,
  },
  {
    heading: "Session",
    path: "settings/session",
    component: SessionSettings,
  },
  {
    heading: "Tags",
    path: "settings/tags",
    component: TagsAndRoutines,
  },
  {
    heading: "Tracking",
    path: "settings/tracking",
    component: TrackingSettings,
  },
  {
    heading: "Data",
    path: "settings/data",
    component: StorageSettings,
  },
  {
    heading: "About",
    path: "settings/about",
    component: AboutSettings,
  },
  {
    heading: "Report feedback",
    path: "settings/footer",
    component: SettingsFooter,
    type: BlockType.INLINE,
  },
  {
    path: "expenseTracker",
    component: ExpenseTracker,
    icon: "Expense Tracker",
  },
];

