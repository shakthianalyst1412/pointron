import type { intervalbar } from "./intervalbar.type";
import type { Preset } from "$lib/local/types/preset.type";
import type { SessionState } from "./sessionState.enum";
import type { SessionType } from "./sessionType.enum";
import type { UserDate } from "$lib/tidy/types/userDate.type";

export type SessionStore = {
  currentSessionId: string | undefined;
  previousSessionId: string | undefined;
  currentBarIndex: number;
  currentBarDuration: number;
  type: SessionType;
  state: SessionState;
  snapshot?: Snapshot;
  selectedPreset?: Preset;
  plannedDuration: number;
  start?: Date;
  end?: Date;
  timeElapsed: number;
  totalElapsed: number;
  totalExtended: number;
  sessionProgress: number;
  bars: intervalbar[];
  blocks: SessionBlock[];
  taskBlocks: TaskBlock[];
  scheduledNotifications: ScheduledNotification[];
};

export type ScheduledNotification = {
  inSeconds: number;
  message: string;
  title?: string;
};

export type Session = {
  elapsed: number;
  extended: number;
  brek: number;
  focus: number;
  start: number;
  end: number;
  id: string;
  label: string;
  intention: string;
  tasks?: Task[];
  blocks: SessionBlock[];
};

export type Snapshot = {
  bars: intervalbar[];
  blocks: SessionBlock[];
};
export type SessionBlock = {
  start: number;
  type: SessionBlockType;
};
export type TaskBlock = {
  start: number;
  previousWorked?: number;
  taskId: string | null;
};
export enum SessionBlockType {
  FOCUS = "FOCUS",
  BREAK = "BREAK",
  IDLE = "IDLE",
}
export type TimePeriod = {
  day: UserDate;
  focus: number;
  sessions: Session[];
  isEmptyPeriod?: boolean;
  startDate?: UserDate;
  endDate?: UserDate;
};

export type Task = {
  id: string;
  label: string;
  estimate: number;
  worked: number;
  checked: boolean;
  tags?: string[];
  sessionId: string;
  order: number;
};
